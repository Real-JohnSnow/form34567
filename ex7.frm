off stat;
dim 3;
index i1,i2,j1,j2,k1,k2; 
local [eps]=e_(i1,j1,k1);
local [eps2]=[eps]*e_(i2,j2,k2);
local [eps3]=[eps]*e_(i1,j2,k2);
local [eps4]=[eps]*e_(i1,j1,k1);
local [eps5]=[eps]*[eps];


.sort
contract;
print;
.end
 
